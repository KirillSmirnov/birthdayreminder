package com.smirnowku.birthdayreminder;

import com.smirnowku.birthdayreminder.core.model.ViewMode;

import java.util.prefs.Preferences;

public class UserPreferences {

    private static final String SOURCE_FILE = "SOURCE_FILE";
    private static final String VIEW_MODE = "VIEW_MODE";

    private final Preferences preferences = Preferences.userNodeForPackage(getClass());

    public String getSourceFile() {
        return preferences.get(SOURCE_FILE, null);
    }

    public void setSourceFile(String sourceFile) {
        setValue(SOURCE_FILE, sourceFile);
    }

    public ViewMode getViewMode() {
        return ViewMode.valueOf(preferences.get(VIEW_MODE, ViewMode.DEFAULT.toString()));
    }

    public void setViewMode(ViewMode viewMode) {
        setValue(VIEW_MODE, viewMode.toString());
    }

    private void setValue(String key, String value) {
        if (value == null) preferences.remove(key);
        else preferences.put(key, value);
    }
}
