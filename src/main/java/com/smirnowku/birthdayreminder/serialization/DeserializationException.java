package com.smirnowku.birthdayreminder.serialization;

public class DeserializationException extends Exception {

    DeserializationException(Throwable cause) {
        super(cause);
    }
}
