package com.smirnowku.birthdayreminder.serialization;

public class SerializerConfigurator {

    public static Serializer provideSerializer() {
        return new SerializerImpl();
    }
}
