package com.smirnowku.birthdayreminder.serialization;

import java.io.File;

public interface Serializer {

    <T> void serialize(T object, File file);

    <T> T deserialize(File file) throws DeserializationException;
}
