package com.smirnowku.birthdayreminder.util;

public class NullChecker {

    public static void ensureNotNullOrEmpty(String s) {
        if (isNullOrEmpty(s))
            throw new IllegalArgumentException("string is empty");
    }

    public static boolean isNullOrEmpty(String s) {
        return s == null || s.isEmpty();
    }
}
