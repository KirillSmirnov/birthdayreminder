package com.smirnowku.birthdayreminder.core;

import com.smirnowku.birthdayreminder.core.model.BirthdayMan;
import com.smirnowku.birthdayreminder.core.model.Person;
import com.smirnowku.birthdayreminder.core.model.ViewMode;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public interface PeopleService {

    Set<Person> getAll();

    void save(Person person);

    void remove(Person person);

    default List<BirthdayMan> getAllBirthdays(ViewMode viewMode) {
        List<BirthdayMan> birthdayMen = getByBirthday(LocalDate.now());
        for (int plusDays = 1; plusDays <= viewMode.getDays(); ++plusDays) {
            LocalDate date = LocalDate.now().plusDays(plusDays);
            birthdayMen.addAll(getByBirthday(date));
        }
        return birthdayMen;
    }

    default List<BirthdayMan> getByBirthday(LocalDate birthdayDate) {
        return getAll().stream()
                .filter(person -> isBirthday(person, birthdayDate))
                .map(person -> BirthdayMan.of(birthdayDate, person))
                .collect(Collectors.toList());
    }

    static boolean isBirthday(Person person, LocalDate onDate) {
        return person.getBirthday() != null &&
                person.getBirthday().getDayOfMonth() == onDate.getDayOfMonth() &&
                person.getBirthday().getMonthValue() == onDate.getMonthValue();
    }
}
