package com.smirnowku.birthdayreminder.core;

import com.smirnowku.birthdayreminder.UserPreferences;

public class DataSourceManager {

    private static final UserPreferences USER_PREFERENCES = new UserPreferences();
    private PeopleService peopleService = createPeopleService();

    public boolean isFileOpened() {
        return peopleService != null;
    }

    public void openFile(String sourceFile) {
        USER_PREFERENCES.setSourceFile(sourceFile);
        peopleService = createPeopleService();
    }

    public void closeFile() {
        USER_PREFERENCES.setSourceFile(null);
        peopleService = null;
    }

    public PeopleService providePeopleService() {
        if (!isFileOpened())
            throw new RuntimeException("PeopleService not initialized");
        return peopleService;
    }

    public String getSourceFile() {
        return USER_PREFERENCES.getSourceFile();
    }

    private PeopleService createPeopleService() {
        String sourceFile = getSourceFile();
        return sourceFile == null ? null : CoreConfigurator.providePeopleService(sourceFile);
    }
}
