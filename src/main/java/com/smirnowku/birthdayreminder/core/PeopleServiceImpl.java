package com.smirnowku.birthdayreminder.core;

import com.smirnowku.birthdayreminder.core.model.Person;

import java.util.Set;

class PeopleServiceImpl implements PeopleService {

    private final Repository repository;

    PeopleServiceImpl(Repository repository) {
        this.repository = repository;
    }

    @Override
    public Set<Person> getAll() {
        return repository.getPeople();
    }

    @Override
    public void save(Person person) {
        Set<Person> people = getAll();
        people.add(person);
        repository.setPeople(people);
    }

    @Override
    public void remove(Person person) {
        Set<Person> people = getAll();
        people.remove(person);
        repository.setPeople(people);
    }
}
