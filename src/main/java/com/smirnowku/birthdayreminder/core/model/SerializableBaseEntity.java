package com.smirnowku.birthdayreminder.core.model;

import java.io.Serializable;

abstract class SerializableBaseEntity extends BaseEntity implements Serializable {
}
