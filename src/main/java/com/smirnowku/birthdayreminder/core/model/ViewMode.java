package com.smirnowku.birthdayreminder.core.model;

public enum ViewMode {

    WEEK(7),
    MONTH(31),
    YEAR(366);

    public static final ViewMode DEFAULT = MONTH;

    private final int days;

    ViewMode(int days) {
        this.days = days;
    }

    public int getDays() {
        return days;
    }
}
