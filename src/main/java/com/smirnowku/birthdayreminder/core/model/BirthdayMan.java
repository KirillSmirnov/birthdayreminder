package com.smirnowku.birthdayreminder.core.model;

import java.time.LocalDate;

public class BirthdayMan {

    private final LocalDate date;
    private final String givenNames;
    private final String surname;
    private final int age;

    public static BirthdayMan of(LocalDate date, Person person) {
        return new BirthdayMan(date, person.getGivenNames(), person.getSurname(),
                date.getYear() - person.getBirthday().getYear());
    }

    private BirthdayMan(LocalDate date, String givenNames, String surname, int age) {
        this.date = date;
        this.givenNames = givenNames;
        this.surname = surname;
        this.age = age;
    }

    public LocalDate getDate() {
        return date;
    }

    public String getGivenNames() {
        return givenNames;
    }

    public String getSurname() {
        return surname;
    }

    public int getAge() {
        return age;
    }
}
