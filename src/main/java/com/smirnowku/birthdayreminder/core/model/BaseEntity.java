package com.smirnowku.birthdayreminder.core.model;

abstract class BaseEntity {

    private static int nextId = 1;

    private final int id;

    BaseEntity() {
        id = nextId++;
    }

    @Override
    public final boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        return id == ((BaseEntity) o).id;
    }

    @Override
    public final int hashCode() {
        return id;
    }
}
