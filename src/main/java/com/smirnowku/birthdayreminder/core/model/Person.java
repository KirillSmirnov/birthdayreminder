package com.smirnowku.birthdayreminder.core.model;

import com.smirnowku.birthdayreminder.util.NullChecker;

import java.time.LocalDate;

public class Person extends SerializableBaseEntity {

    private String surname;
    private String givenNames;
    private LocalDate birthday;

    public Person(String surname, String givenNames, LocalDate birthday) {
        setSurname(surname);
        setGivenNames(givenNames);
        setBirthday(birthday);
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getGivenNames() {
        return givenNames;
    }

    public void setGivenNames(String givenNames) {
        NullChecker.ensureNotNullOrEmpty(givenNames);
        this.givenNames = givenNames;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    @Override
    public String toString() {
        return "Person{" +
                "surname='" + surname + '\'' +
                ", givenNames='" + givenNames + '\'' +
                ", birthday=" + birthday +
                '}';
    }
}
