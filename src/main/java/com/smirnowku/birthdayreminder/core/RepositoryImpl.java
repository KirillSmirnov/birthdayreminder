package com.smirnowku.birthdayreminder.core;

import com.smirnowku.birthdayreminder.core.model.Person;
import com.smirnowku.birthdayreminder.serialization.DeserializationException;
import com.smirnowku.birthdayreminder.serialization.Serializer;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

class RepositoryImpl implements Repository {

    private final File dataSource;
    private final Serializer serializer;
    private Set<Person> people;

    RepositoryImpl(File dataSource, Serializer serializer) {
        this.dataSource = dataSource;
        this.serializer = serializer;
        readData();
    }

    @Override
    public Set<Person> getPeople() {
        return people;
    }

    @Override
    public void setPeople(Set<Person> people) {
        this.people = people;
        writeData();
    }

    private void readData() {
        try {
            people = serializer.deserialize(dataSource);
        } catch (DeserializationException e) {
            people = new HashSet<>();
        }
    }

    private void writeData() {
        serializer.serialize(people, dataSource);
    }
}
