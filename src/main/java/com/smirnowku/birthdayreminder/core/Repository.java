package com.smirnowku.birthdayreminder.core;

import com.smirnowku.birthdayreminder.core.model.Person;

import java.util.Set;

interface Repository {

    Set<Person> getPeople();

    void setPeople(Set<Person> people);
}
