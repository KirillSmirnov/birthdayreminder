package com.smirnowku.birthdayreminder.core;

import com.smirnowku.birthdayreminder.serialization.Serializer;
import com.smirnowku.birthdayreminder.serialization.SerializerConfigurator;

import java.io.File;

class CoreConfigurator {

    static PeopleService providePeopleService(String sourceFile) {
        return new PeopleServiceImpl(provideRepository(sourceFile));
    }

    private static Repository provideRepository(String sourceFile) {
        return new RepositoryImpl(new File(sourceFile), provideSerializer());
    }

    private static Serializer provideSerializer() {
        return SerializerConfigurator.provideSerializer();
    }
}
