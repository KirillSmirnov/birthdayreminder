package com.smirnowku.birthdayreminder.fx.controller;

import com.smirnowku.birthdayreminder.core.model.BirthdayMan;
import com.smirnowku.birthdayreminder.core.model.ViewMode;
import com.smirnowku.birthdayreminder.fx.Layouts;
import com.smirnowku.birthdayreminder.fx.StageBuilder;
import javafx.collections.FXCollections;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.stage.FileChooser;

import java.io.File;
import java.net.URL;
import java.time.LocalDate;
import java.util.List;
import java.util.ResourceBundle;

public class MainController extends FXMLController {

    public MenuItem editFileButton;
    public MenuItem closeFileButton;
    public RadioMenuItem weekViewButton;
    public RadioMenuItem monthViewButton;
    public RadioMenuItem yearViewButton;
    public Label statusLabel;

    public TableView<BirthdayMan> table;
    public TableColumn<BirthdayMan, LocalDate> dateColumn;
    public TableColumn<BirthdayMan, String> givenNamesColumn;
    public TableColumn<BirthdayMan, String> surnameColumn;
    public TableColumn<BirthdayMan, Integer> ageColumn;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        dateColumn.setCellFactory(TextFieldTableCell.forTableColumn(new PrettyLocalDateConverter()));
        dateColumn.setCellValueFactory(new PropertyValueFactory<>("date"));
        givenNamesColumn.setCellValueFactory(new PropertyValueFactory<>("givenNames"));
        surnameColumn.setCellValueFactory(new PropertyValueFactory<>("surname"));
        ageColumn.setCellValueFactory(new PropertyValueFactory<>("age"));
        refresh();
    }

    public void newFile() {
        chooseFileToOpen("New data file", false);
    }

    public void openFile() {
        chooseFileToOpen("Open data file", true);
    }

    public void editFile() {
        StageBuilder stageBuilder = new StageBuilder();
        stageBuilder.initialize(Layouts.EDIT)
                .setOnCloseRequest(windowEvent -> refreshData());
        stageBuilder.showDialog();
    }

    public void closeFile() {
        getDataSourceManager().closeFile();
        refresh();
    }

    public void onViewChanged() {
        getUserPreferences().setViewMode(getViewMode());
        refreshData();
    }

    private void refresh() {
        boolean fileOpened = getDataSourceManager().isFileOpened();
        statusLabel.setText(fileOpened ? getDataSourceManager().getSourceFile() : "No file");
        editFileButton.setDisable(!fileOpened);
        closeFileButton.setDisable(!fileOpened);
        setViewMode(getUserPreferences().getViewMode());
        refreshData();
    }

    private void refreshData() {
        table.setItems(null);
        if (!getDataSourceManager().isFileOpened()) return;
        List<BirthdayMan> birthdayMen = getDataSourceManager().providePeopleService().getAllBirthdays(getViewMode());
        table.setItems(FXCollections.observableList(birthdayMen));
    }

    private ViewMode getViewMode() {
        if (weekViewButton.isSelected()) return ViewMode.WEEK;
        if (monthViewButton.isSelected()) return ViewMode.MONTH;
        if (yearViewButton.isSelected()) return ViewMode.YEAR;
        throw new RuntimeException();
    }

    private void setViewMode(ViewMode viewMode) {
        weekViewButton.setSelected(viewMode == ViewMode.WEEK);
        monthViewButton.setSelected(viewMode == ViewMode.MONTH);
        yearViewButton.setSelected(viewMode == ViewMode.YEAR);
    }

    private void chooseFileToOpen(String title, boolean exists) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle(title);
        File sourceFile = exists ? fileChooser.showOpenDialog(getPrimaryStage())
                : fileChooser.showSaveDialog(getPrimaryStage());
        if (sourceFile != null) {
            getDataSourceManager().openFile(sourceFile.getAbsolutePath());
            refresh();
        }
    }
}
