package com.smirnowku.birthdayreminder.fx.controller;

import com.smirnowku.birthdayreminder.util.NullChecker;
import javafx.util.StringConverter;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;

class LocalDateConverter extends StringConverter<LocalDate> {

    private boolean parsingError = false;

    @Override
    public String toString(LocalDate date) {
        return date == null ? null : date.toString();
    }

    @Override
    public LocalDate fromString(String s) {
        parsingError = false;
        if (!NullChecker.isNullOrEmpty(s))
            try {
                return LocalDate.parse(s);
            } catch (DateTimeParseException e) {
                parsingError = true;
            }
        return null;
    }

    boolean parsingError() {
        return parsingError;
    }
}
