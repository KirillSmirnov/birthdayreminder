package com.smirnowku.birthdayreminder.fx.controller;

import javafx.util.StringConverter;

import java.time.LocalDate;

class PrettyLocalDateConverter extends StringConverter<LocalDate> {

    @Override
    public String toString(LocalDate date) {
        if (date.equals(LocalDate.now())) return "TODAY";
        if (date.equals(LocalDate.now().plusDays(1))) return "Tomorrow";
        return String.format("%s %d", formatMonth(date.getMonth().toString()), date.getDayOfMonth());
    }

    @Override
    public LocalDate fromString(String s) {
        return null;
    }

    private String formatMonth(String month) {
        return month.substring(0, 1).toUpperCase()
                .concat(month.substring(1).toLowerCase());
    }
}
