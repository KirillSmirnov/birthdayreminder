package com.smirnowku.birthdayreminder.fx.controller;

import com.smirnowku.birthdayreminder.core.PeopleService;
import com.smirnowku.birthdayreminder.core.model.Person;
import com.smirnowku.birthdayreminder.fx.Notification;
import com.smirnowku.birthdayreminder.fx.dialog.ConfirmDialog;
import com.smirnowku.birthdayreminder.util.NullChecker;
import javafx.collections.FXCollections;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;

import java.net.URL;
import java.time.LocalDate;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

public class EditController extends FXMLController {

    public TableView<Person> table;
    public TableColumn<Person, String> givenNamesColumn;
    public TableColumn<Person, String> surnameColumn;
    public TableColumn<Person, LocalDate> dateOfBirthColumn;

    public TextField givenNamesBox;
    public TextField surnameBox;
    public TextField dateOfBirthBox;

    public Label notificationLabel;

    private final PeopleService peopleService = getDataSourceManager().providePeopleService();
    private final LocalDateConverter localDateConverter = new LocalDateConverter();

    private Notification notification;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        notification = new Notification(notificationLabel);

        givenNamesColumn.setCellValueFactory(new PropertyValueFactory<>("givenNames"));
        surnameColumn.setCellValueFactory(new PropertyValueFactory<>("surname"));
        dateOfBirthColumn.setCellValueFactory(new PropertyValueFactory<>("birthday"));

        givenNamesColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        surnameColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        dateOfBirthColumn.setCellFactory(TextFieldTableCell.forTableColumn(localDateConverter));

        table.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        refreshData();
    }

    public void onGivenNamesCommit(TableColumn.CellEditEvent<Person, String> personStringCellEditEvent) {
        String givenNames = personStringCellEditEvent.getNewValue();
        if (NullChecker.isNullOrEmpty(givenNames)) {
            notification.show("You must enter given names");
            table.refresh();
            return;
        }
        Person person = personStringCellEditEvent.getRowValue();
        person.setGivenNames(givenNames);
        peopleService.save(person);
    }

    public void onSurnameCommit(TableColumn.CellEditEvent<Person, String> personStringCellEditEvent) {
        Person person = personStringCellEditEvent.getRowValue();
        person.setSurname(personStringCellEditEvent.getNewValue());
        peopleService.save(person);
    }

    public void onBirthdayCommit(TableColumn.CellEditEvent<Person, LocalDate> personLocalDateCellEditEvent) {
        LocalDate birthday = personLocalDateCellEditEvent.getNewValue();
        if (localDateConverter.parsingError()) {
            notification.show("Follow this date format: YYYY-MM-DD");
            table.refresh();
            return;
        }
        Person person = personLocalDateCellEditEvent.getRowValue();
        person.setBirthday(birthday);
        peopleService.save(person);
    }

    public void addRecord() {
        String givenNames = givenNamesBox.getText();
        String surname = surnameBox.getText();
        LocalDate birthday = localDateConverter.fromString(dateOfBirthBox.getText());
        if (NullChecker.isNullOrEmpty(givenNames)) {
            notification.show("You must enter given names");
            return;
        }
        if (localDateConverter.parsingError()) {
            notification.show("Follow this date format: YYYY-MM-DD");
            return;
        }
        Person newPerson = new Person(surname, givenNames, birthday);
        peopleService.save(newPerson);
        refreshData();
        clearAddFields();
    }

    public void removeRecords() {
        List<Person> peopleToRemove = table.getSelectionModel().getSelectedItems();
        if (peopleToRemove.isEmpty()) {
            notification.show("Select records to remove");
            return;
        }
        String recordsToRemove = String.join("\n", peopleToRemove.stream()
                .map(person -> String.join(" ", person.getGivenNames(),
                        person.getSurname() == null ? "" : person.getSurname()))
                .collect(Collectors.toList()));
        new ConfirmDialog("Remove records", "Are you sure you want to remove these records?\n" +
                recordsToRemove, "Remove", () -> peopleToRemove.forEach(peopleService::remove))
                .show();
        refreshData();
    }

    private void refreshData() {
        table.setItems(FXCollections.observableArrayList(peopleService.getAll()));
    }

    private void clearAddFields() {
        givenNamesBox.clear();
        surnameBox.clear();
        dateOfBirthBox.clear();
    }
}
