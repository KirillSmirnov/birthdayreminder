package com.smirnowku.birthdayreminder.fx.controller;

import com.smirnowku.birthdayreminder.UserPreferences;
import com.smirnowku.birthdayreminder.core.DataSourceManager;
import com.smirnowku.birthdayreminder.fx.MainApp;
import javafx.stage.Stage;

abstract class Controller {

    private static final UserPreferences USER_PREFERENCES = new UserPreferences();
    private static final DataSourceManager DATA_SOURCE_MANAGER = new DataSourceManager();

    public void closeApp() {
        getPrimaryStage().close();
    }

    Stage getPrimaryStage() {
        return MainApp.get().getPrimaryStage();
    }

    UserPreferences getUserPreferences() {
        return USER_PREFERENCES;
    }

    DataSourceManager getDataSourceManager() {
        return DATA_SOURCE_MANAGER;
    }
}
