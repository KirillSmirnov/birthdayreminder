package com.smirnowku.birthdayreminder.fx;

public class Layouts {

    //root
    public static final StageProperty MAIN = new StageProperty("/layout/root/main.fxml", true, true);
    public static final StageProperty EDIT = new StageProperty("/layout/root/edit.fxml", true, true);

    //dialog
    public static final StageProperty CONFIRM_DIALOG = new StageProperty("/layout/dialog/confirm.fxml", false, true);
}
