package com.smirnowku.birthdayreminder.fx;

import javafx.scene.image.Image;

public class StageProperty {

    private static final Image APP_ICON = new Image("/icon/gift.png");
    private static final String APP_TITLE = "Birthday Reminder";

    private final String sceneLayoutPath;
    private final boolean resizable;
    private final boolean centered;

    StageProperty(String sceneLayoutPath, boolean resizable, boolean centered) {
        this.sceneLayoutPath = sceneLayoutPath;
        this.resizable = resizable;
        this.centered = centered;
    }

    Image getIcon() {
        return APP_ICON;
    }

    String getTitle() {
        return APP_TITLE;
    }

    String getSceneLayoutPath() {
        return sceneLayoutPath;
    }

    boolean isResizable() {
        return resizable;
    }

    boolean isCentered() {
        return centered;
    }
}
