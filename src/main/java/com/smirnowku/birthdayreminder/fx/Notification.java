package com.smirnowku.birthdayreminder.fx;

import javafx.concurrent.Task;
import javafx.scene.control.Label;

public class Notification {

    private static final int DELAY = 2000;
    private final Label notificationLabel;
    private Thread waitAndClearThread;

    public Notification(Label notificationLabel) {
        this.notificationLabel = notificationLabel;
    }

    public void show(String message) {
        hide();
        startTask(message);
    }

    public void hide() {
        if (waitAndClearThread != null)
            waitAndClearThread.interrupt();
    }

    private void startTask(String message) {
        waitAndClearThread = new Thread(new WaitAndClearTask(message));
        waitAndClearThread.start();
    }

    private class WaitAndClearTask extends Task<Void> {

        private final String message;

        WaitAndClearTask(String message) {
            this.message = message;
        }

        @Override
        protected void running() {
            notificationLabel.setText(message);
        }

        @Override
        protected Void call() throws Exception {
            Thread.sleep(DELAY);
            return null;
        }

        @Override
        protected void succeeded() {
            notificationLabel.setText(null);
        }

        @Override
        protected void failed() {
            notificationLabel.setText(null);
        }
    }
}
