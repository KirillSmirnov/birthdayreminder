package com.smirnowku.birthdayreminder.fx;

import com.smirnowku.birthdayreminder.UserPreferences;
import com.smirnowku.birthdayreminder.core.DataSourceManager;
import com.smirnowku.birthdayreminder.core.PeopleService;
import com.smirnowku.birthdayreminder.core.model.BirthdayMan;
import javafx.application.Application;
import javafx.stage.Stage;

import java.util.List;

public class MainApp extends Application {

    private static MainApp app;
    private Stage primaryStage;

    public static void main(String[] args) {
        if (args.length > 0 && args[0].equals("startup")) {
            if (!hasDataToDisplay()) System.exit(0);
        }
        launch(args);
    }

    public static MainApp get() {
        return app;
    }

    public MainApp() {
        MainApp.app = this;
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = new StageBuilder(primaryStage).initialize(Layouts.MAIN);
        this.primaryStage.show();
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }

    private static boolean hasDataToDisplay() {
        DataSourceManager dataSourceManager = new DataSourceManager();
        if (!dataSourceManager.isFileOpened()) return false;
        PeopleService peopleService = dataSourceManager.providePeopleService();
        UserPreferences userPreferences = new UserPreferences();
        List<BirthdayMan> birthdayMen = peopleService.getAllBirthdays(userPreferences.getViewMode());
        return !birthdayMen.isEmpty();
    }
}
