package com.smirnowku.birthdayreminder.fx;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class StageBuilder {

    private final Stage stage;

    public StageBuilder() {
        stage = new Stage();
    }

    public StageBuilder(Stage stage) {
        this.stage = stage;
    }

    public Stage initialize(StageProperty stageProperty) {
        stage.setScene(new Scene(loadParent(stageProperty.getSceneLayoutPath())));
        stage.getIcons().add(stageProperty.getIcon());
        stage.setTitle(stageProperty.getTitle());
        stage.setResizable(stageProperty.isResizable());
        if (stageProperty.isCentered())
            stage.centerOnScreen();
        return stage;
    }

    public void showDialog() {
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.showAndWait();
    }

    private static Parent loadParent(String layoutPath) {
        try {
            return FXMLLoader.load(StageBuilder.class.getResource(layoutPath));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
