import com.smirnowku.birthdayreminder.core.DataSourceManager;
import com.smirnowku.birthdayreminder.core.PeopleService;
import com.smirnowku.birthdayreminder.core.model.Person;
import org.junit.After;
import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Iterator;
import java.util.Set;

import static org.junit.Assert.assertEquals;

public class MainTest extends BaseTest {

    private final DataSourceManager dataSourceManager;
    private final PeopleService peopleService;
    private final String sf0;

    public MainTest() {
        dataSourceManager = new DataSourceManager();
        sf0 = dataSourceManager.getSourceFile();
        dataSourceManager.openFile("./test.data");
        peopleService = dataSourceManager.providePeopleService();
    }

    @After
    public void revert() {
        dataSourceManager.closeFile();
        if (sf0 != null) dataSourceManager.openFile(sf0);
    }

    @Test
    public void addPerson() {
        print("\n\nAdd Person");
        print("Before:");
        showAll();

        Person person = new Person(null, LocalDateTime.now().toString(), LocalDate.now());
        int count0 = peopleService.getAll().size();
        peopleService.save(person);
        assertEquals(count0 + 1, peopleService.getAll().size());

        print("After:");
        showAll();
    }

    @Test
    public void updatePerson() {
        print("\n\nUpdate Person");
        print("Before:");
        showAll();

        Set<Person> people = peopleService.getAll();
        int count0 = people.size();
        Iterator<Person> it = people.iterator();
        if (!it.hasNext()) {
            System.out.println("no people");
            return;
        }
        Person person = it.next();
        person.setSurname("Updated on " + LocalDateTime.now());
        peopleService.save(person);
        assertEquals(count0, peopleService.getAll().size());

        print("After:");
        showAll();
    }

    @Test
    public void removePerson() {
        print("\n\nRemove Person");
        print("Before:");
        showAll();

        Set<Person> people = peopleService.getAll();
        int count0 = people.size();
        Iterator<Person> it = people.iterator();
        if (!it.hasNext()) {
            print("no people");
            return;
        }
        peopleService.remove(it.next());
        assertEquals(count0 - 1, peopleService.getAll().size());

        print("After:");
        showAll();
    }

    @Test
    public void showAll() {
        peopleService.getAll().stream().map(Object::toString).forEach(this::print);
        print("*********\n\n");
    }
}
