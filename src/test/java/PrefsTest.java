import com.smirnowku.birthdayreminder.UserPreferences;
import com.smirnowku.birthdayreminder.core.model.ViewMode;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PrefsTest extends BaseTest {

    private static final UserPreferences USER_PREFERENCES = new UserPreferences();

    @Test
    public void testSourceFile() {
        String s0 = USER_PREFERENCES.getSourceFile();
        print("path = " + s0);

        String s1 = "./test.data";
        USER_PREFERENCES.setSourceFile(s1);
        String s2 = USER_PREFERENCES.getSourceFile();
        assertEquals(s1, s2);

        USER_PREFERENCES.setSourceFile(s0);
    }

    @Test
    public void testViewMode() {
        ViewMode v0 = USER_PREFERENCES.getViewMode();
        print("view = " + v0);

        ViewMode v1 = ViewMode.WEEK;
        USER_PREFERENCES.setViewMode(v1);
        ViewMode v2 = USER_PREFERENCES.getViewMode();
        assertEquals(v1, v2);

        USER_PREFERENCES.setViewMode(v0);
    }
}
